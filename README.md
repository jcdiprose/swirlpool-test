# PROTOTYPE TEST JOB - CALUM DIPROSE

Duplicate data in json was removed to avoid duplicate key clash

```
    {
      "measurement_type_id": "air_pressure",
      "sensor_type_id": "barometer"
    },
```

Using Chakra UI, React, Typescript & immer

Bootstrapped with create-react-app
