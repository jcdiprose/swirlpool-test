import React from 'react'
import PrototypePage from './pages/prototype'
import { ChakraProvider } from '@chakra-ui/react'
import theme from './theme'
import '@fontsource/roboto' // Defaults to weight 400.

function App() {
  return (
    <ChakraProvider theme={theme}>
      <div className="App">
        <PrototypePage />
      </div>
    </ChakraProvider>
  )
}

export default App
