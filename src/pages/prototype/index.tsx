import React, { useReducer } from 'react'
import { reducer, initialState } from '../../reducers/prototypeReducer'
import { Grid, GridItem } from '@chakra-ui/react'

import UncategorizedColumns from '../../components/uncategorizedColumns'
import ToCategorize from '../../components/toCategorize'
import CategorizedList from '../../components/categorizedList'

const PrototypePage = () => {
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <>
      <Grid
        minH="600px"
        templateRows="repeat(3, 1fr)"
        templateColumns="210px repeat(4, 1fr)"
        gap="30px"
        pt="40px"
        pl="35px"
        pr="36px"
        bg="#e5e5e5"
      >
        <GridItem rowSpan={3} colSpan={1}>
          <UncategorizedColumns
            columns={state.uncategorized_columns}
            selected={state.to_categorize}
            dispatch={dispatch}
          />
        </GridItem>
        <GridItem colSpan={4}>
          <ToCategorize
            columns={state.to_categorize}
            dispatch={dispatch}
            sensorTypeIds={state.sensor_type_ids}
            measurementTypesPerSensor={state.measurement_types_per_sensor}
          />
          {state.categorized.length > 0 && (
            <CategorizedList categorized={state.categorized} />
          )}
        </GridItem>
      </Grid>
    </>
  )
}

export default PrototypePage
