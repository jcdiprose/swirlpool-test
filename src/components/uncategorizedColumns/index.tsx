import React, { Dispatch } from 'react'
import { List, Flex, Checkbox, Box, Text } from '@chakra-ui/react'
import { SET_TO_CATEGORIZE } from '../../reducers/prototypeReducer'

interface Props {
  columns: string[]
  selected: string[]
  dispatch: Dispatch<Record<string, unknown>>
}

const UncategorizedColumns = ({ columns, selected, dispatch }: Props) => {
  return (
    <List w="210px" shadow="base">
      <Box>
        <Text
          bg="darkblue"
          borderTopRadius="2px"
          boxShadow="2px 9px 47px rgba(0, 0, 0, 0.02);"
          color="white"
          h="40px"
          pt="15px"
          pb="12px"
          pl="15px"
          fontSize="12px"
          lineHeight="13px"
        >
          UNCATEGORIZED COLUMNS
        </Text>
      </Box>
      {columns.map((title) => (
        <Flex
          key={title}
          bg="white"
          alignItems="center"
          px="15.83px"
          py="10.83px"
          borderBottom="1px"
          borderBottomRadius="2px"
          borderStyle="solid"
          borderColor="lightgray"
        >
          <Checkbox
            value={title}
            isChecked={!!selected.find((col) => col === title)}
            onChange={(e) =>
              dispatch({ type: SET_TO_CATEGORIZE, payload: e.target.value })
            }
            colorScheme="swirlpoolgreen"
            size="sm"
          >
            <Text fontSize="12px" lineHeight="14px" color="bluegray">
              {title}
            </Text>
          </Checkbox>
        </Flex>
      ))}
    </List>
  )
}

export default UncategorizedColumns
