import React, { Dispatch, useState } from 'react'
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Flex,
  Box,
  Checkbox,
  Select,
  Input,
  Button,
  Text,
} from '@chakra-ui/react'
import StyledTh, { StyledThTL } from '../atoms/styledTh'
import { SET_TO_CATEGORIZE, SET_NEW_CATEGORIZED } from '../../reducers/prototypeReducer'

interface Props {
  columns: string[]
  dispatch: Dispatch<Record<string, unknown>>
  measurementTypesPerSensor: Record<string, string>[]
  sensorTypeIds: string[]
}

interface Statistics {
  [key: string]: Statistic
}

const ToCategorize = ({
  columns,
  measurementTypesPerSensor,
  sensorTypeIds,
  dispatch,
}: Props) => {
  const [sensor, setSensor] = useState<string>('')
  const [measurement, setMeasurement] = useState<string>('')
  const [height, setHeight] = useState<string>('')
  const [boom, setBoom] = useState<string>('')
  const [statistics, setStatistics] = useState<Statistics>({})

  const sensorChange = (e: any) => setSensor(e.target.value)
  const measurementChange = (e: any) => setMeasurement(e.target.value)
  const heightChange = (e: any) => {
    const value = e.target.value
    setHeight(value < 0 ? 0 : value > 150 ? 150 : value)
  }
  const boomChange = (e: any) => {
    const value = e.target.value
    setBoom(value < 0 ? 0 : value > 360 ? 360 : value)
  }
  const statisticChange = (column: string, e: any) => {
    setStatistics({
      ...statistics,
      [column]: e.target.value,
    })
  }

  const create = () => {
    dispatch({
      type: SET_NEW_CATEGORIZED,
      payload: {
        id: Date.now(),
        columns,
        sensor,
        measurement,
        height: parseInt(height),
        boom: parseInt(boom),
        statistics,
      },
    })
    clear()
  }

  const clear = () => {
    setSensor('')
    setMeasurement('')
    setHeight('')
    setBoom('')
    setStatistics({})
  }

  const allDataSelected = !(
    sensor &&
    measurement &&
    height &&
    boom &&
    Object.keys(statistics).length === columns.length
  )

  return (
    <Table bg="white" shadow="base">
      <Thead>
        <Tr textTransform="uppercase">
          <StyledThTL w="210px">column name</StyledThTL>
          <StyledTh w="140px">sensor</StyledTh>
          <StyledTh w="140px">measurement</StyledTh>
          <StyledTh w="90px">height</StyledTh>
          <StyledTh w="90px">boom</StyledTh>
          <StyledTh w="160px">statistic</StyledTh>
          <Th bg="darkblue" borderTopRightRadius="2px" />
        </Tr>
      </Thead>
      <Tbody>
        {columns.length > 0 && (
          <Tr>
            <Td
              px="0"
              py="0"
              borderRight="1px"
              borderStyle="solid"
              borderRightColor="lightgray"
            >
              {columns.map((column) => (
                <Box
                  borderBottom="1px"
                  borderStyle="solid"
                  borderColor="lightgray"
                  _last={{ borderBottom: '0px' }}
                >
                  <Flex pl="15px" alignItems="center" key={column} h="39px">
                    <Checkbox
                      value={column}
                      isChecked={true}
                      onChange={(e) =>
                        dispatch({ type: SET_TO_CATEGORIZE, payload: e.target.value })
                      }
                      size="sm"
                      colorScheme="swirlpoolgreen"
                    >
                      <Text fontSize="12px" lineHeight="14px" color="bluegray" px="12px">
                        {column}
                      </Text>
                    </Checkbox>
                  </Flex>
                </Box>
              ))}
            </Td>
            <Td px="15px" py="14px">
              <Select
                placeholder="-"
                cursor="pointer"
                onChange={sensorChange}
                value={sensor}
                fontSize="12px"
                h="28px"
              >
                {sensorTypeIds.map((id) => (
                  <option key={id} value={id}>
                    {id}
                  </option>
                ))}
              </Select>
            </Td>
            <Td px="15px" py="14px">
              <Select
                placeholder="-"
                cursor="pointer"
                onChange={measurementChange}
                value={measurement}
                fontSize="12px"
                h="28px"
              >
                {measurementTypesPerSensor
                  .filter((mtps) => mtps.sensor_type_id === sensor)
                  .map((mtps) => (
                    <option
                      key={mtps.measurement_type_id}
                      value={mtps.measurement_type_id}
                    >
                      {mtps.measurement_type_id}
                    </option>
                  ))}
              </Select>
            </Td>
            <Td px="15px" py="14px">
              <Input
                type="number"
                min={0}
                max={150}
                value={height}
                onChange={heightChange}
                placeholder="-"
                fontSize="12px"
                h="28px"
              />
            </Td>
            <Td px="15px" py="14px">
              <Input
                type="number"
                min={0}
                max={360}
                value={boom}
                onChange={boomChange}
                placeholder="-"
                fontSize="12px"
                h="28px"
              />
            </Td>
            <Td px="15px" py="0" flexDir="column">
              {columns.map((column) => (
                <Flex h="39px" alignItems="center" justifyContent="center">
                  <Select
                    placeholder="-"
                    cursor="pointer"
                    onChange={(e) => statisticChange(column, e)}
                    value={statistics[column] ? statistics[column] : ''}
                    fontSize="12px"
                    h="28px"
                    key={`${column}statistic`}
                  >
                    <option value="avg">AVG</option>
                    <option value="std">STD</option>
                    <option value="min">MIN</option>
                    <option value="max">MAX</option>
                  </Select>
                </Flex>
              ))}
            </Td>
            <Td>
              <Box
                borderLeft="1px"
                borderStyle="solid"
                borderLeftColor="lightestgray"
                py="20px"
              >
                <Button
                  colorScheme="swirlpoolgreen"
                  ml="40px"
                  px="40px"
                  disabled={allDataSelected}
                  onClick={create}
                >
                  Create
                </Button>
                <Button
                  bg="rgba(30,36,71,0.07)"
                  color="rgba(30,36,71,0.5)"
                  ml="12px"
                  px="25px"
                  onClick={clear}
                >
                  Clear
                </Button>
              </Box>
            </Td>
          </Tr>
        )}
      </Tbody>
    </Table>
  )
}

export default ToCategorize
