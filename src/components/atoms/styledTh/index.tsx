import React, { ReactNode } from 'react'
import { Td, Th } from '@chakra-ui/react'

interface Props {
  w?: string | number
  children?: ReactNode | ReactNode[]
}

const StyledTh = ({ w, children }: Props) => {
  return (
    <Th px="15px" py="14px" w={w} color="white" bg="darkblue">
      {children}
    </Th>
  )
}

export const StyledThTL = ({ w, children }: Props) => {
  return (
    <Th px="15px" py="14px" w={w} color="white" bg="darkblue" borderTopLeftRadius="2px">
      {children}
    </Th>
  )
}

export const StyledTd = ({ children }: Props) => {
  return (
    <Td px="15px" py="14px" borderColor="lightgray" borderBottomColor="lightgray">
      {children}
    </Td>
  )
}
export default StyledTh
