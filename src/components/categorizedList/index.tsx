import React from 'react'
import {
  Table,
  Thead,
  Tbody,
  Tr,
  Td,
  Text,
  Heading,
  Flex,
  Checkbox,
} from '@chakra-ui/react'
import StyledTh, { StyledTd, StyledThTL } from '../atoms/styledTh'

interface Props {
  categorized: Categorized[]
}

const CategorizedList = ({ categorized }: Props) => {
  return (
    <Flex flexDir="column">
      <Flex flexDir="row" justifyContent="flex-end" mt="41px">
        <Checkbox colorScheme="swirlpoolgreen" size="md">
          <Text fontSize="13px" lineHeight="16px" color="bluegray" mr="26px">
            Sensor History
          </Text>
        </Checkbox>
        <Checkbox colorScheme="swirlpoolgreen" size="md">
          <Text fontSize="13px" lineHeight="16px" color="bluegray" mr="26px">
            Logger Slope & Offset
          </Text>
        </Checkbox>
        <Checkbox colorScheme="swirlpoolgreen" size="md">
          <Text fontSize="13px" lineHeight="16px" color="bluegray">
            Calibration
          </Text>
        </Checkbox>
      </Flex>
      <Table mt="26px" shadow="base">
        <Thead>
          <Tr>
            <StyledThTL w="60px" />
            <StyledTh>Column Name</StyledTh>
            <StyledTh>Measurement</StyledTh>
            <StyledTh>Height</StyledTh>
            <StyledTh>Boom</StyledTh>
            <StyledTh>Statistic</StyledTh>
            <StyledTh>Sensor History</StyledTh>
            <StyledTh>Slope & Offset</StyledTh>
          </Tr>
        </Thead>
        <Tbody bg="white">
          {categorized.map((cat) => (
            <Tr key={cat.id} borderBottom="0px">
              <Td
                pos="relative"
                padding="0"
                borderRight="1px"
                borderRightColor="lightgray"
                borderColor="lightgray"
                borderBottomColor="lightgray"
              >
                <Heading
                  border="none"
                  transform="rotate(180deg)"
                  style={{ writingMode: 'vertical-lr' }}
                  fontSize="14px"
                  lineHeight="14px"
                  ml="23px"
                  py="12px"
                >
                  hi
                  {cat.sensor}
                </Heading>
              </Td>
              <StyledTd>
                {cat.columns.map((column) => (
                  <Flex
                    h="30px"
                    fontSize="12px"
                    lineHeight="14px"
                    key={column}
                    alignItems="center"
                  >
                    {column}
                  </Flex>
                ))}
              </StyledTd>
              <StyledTd>
                <Text fontSize="13px" lineHeight="14px">
                  {cat.measurement}
                </Text>
              </StyledTd>
              <StyledTd>
                <Text fontSize="13px" lineHeight="14px">
                  {cat.height}
                </Text>
              </StyledTd>
              <StyledTd>
                <Text fontSize="13px" lineHeight="14px">
                  {cat.boom}
                </Text>
              </StyledTd>
              <StyledTd>
                {cat.columns.map((column) => (
                  <Flex
                    key={cat.statistics[column]}
                    alignItems="center"
                    h="30px"
                    fontSize="13px"
                    lineHeight="14px"
                  >
                    {cat.statistics[column]}
                  </Flex>
                ))}
              </StyledTd>
              <Td borderColor="lightgray" borderBottomColor="lightgray" />
              <Td borderColor="lightgray" borderBottomColor="lightgray" />
            </Tr>
          ))}
        </Tbody>
      </Table>
    </Flex>
  )
}

export default CategorizedList
