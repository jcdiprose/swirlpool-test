import { extendTheme } from '@chakra-ui/react'

const theme = extendTheme({
  fonts: {
    heading: 'Roboto',
    body: 'Roboto',
  },
  colors: {
    lightgray: 'rgba(30,36,71,0.15)',
    lightestgray: 'rgba(30,36,71,0.05)',
    bluegray: '#1E2447',
    darkblue: '#111B30',
    swirlpoolgreen: {
      500: '#00C48C',
      600: '#00C48C',
    },
  },
  components: {
    Checkbox: {
      baseStyle: {
        control: {
          borderColor: 'bluegray',
          opacity: 0.85,
          borderWidth: '1px',
          borderRadius: '2px',
        },
      },
    },
    Table: {
      baseStyle: {
        th: {
          fontWeight: 'normal',
        },
      },
    },
  },
})

export default theme
