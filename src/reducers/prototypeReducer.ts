import { produce } from 'immer'
import json from '../columns_prototype.json'

interface InitialState {
  uncategorized_columns: string[]
  measurement_types_per_sensor: Record<string, string>[]
  sensor_type_ids: string[]
  to_categorize: string[]
  categorized: Categorized[]
}

export const initialState: InitialState = {
  uncategorized_columns: json.uncategorized_columns,
  measurement_types_per_sensor: json.measurement_types_per_sensor,
  sensor_type_ids: (() => {
    const sensor_type_ids: Record<string, undefined> = {}
    json.measurement_types_per_sensor.forEach((types_per_sensor) => {
      sensor_type_ids[types_per_sensor.sensor_type_id] = undefined
    })
    return Object.keys(sensor_type_ids)
  })(),
  to_categorize: [],
  categorized: [],
}

export const SET_TO_CATEGORIZE = 'set_to_categorize'
export const SET_NEW_CATEGORIZED = 'set_new_categorized'

export const reducer = produce((draft, action) => {
  switch (action.type) {
    case SET_TO_CATEGORIZE:
      const index = draft.to_categorize.findIndex(
        (item: string) => item === action.payload
      )
      if (index !== -1) {
        draft.to_categorize = draft.to_categorize.filter(
          (item: string) => item !== action.payload
        )
      } else {
        draft.to_categorize.push(action.payload)
      }
      break

    case SET_NEW_CATEGORIZED:
      draft.categorized.push(action.payload)
      break
    default:
      throw Error('No matching action type')
  }
})
