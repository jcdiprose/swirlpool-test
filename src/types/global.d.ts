type Statistic = 'avg' | 'std' | 'min' | 'max'

interface Categorized {
  id: number
  columns: string[]
  sensor: string
  measurement: string
  height: number
  boom: number
  statistics: Statistics
}
